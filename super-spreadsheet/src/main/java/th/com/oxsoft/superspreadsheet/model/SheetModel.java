package th.com.oxsoft.superspreadsheet.model;

import java.util.Map;

import lombok.Data;

@Data
public class SheetModel {
    private Map<Integer, Map<Integer, RowModel>> sheet;
}
