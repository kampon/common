package th.com.oxsoft.superspreadsheet.model;

import java.util.Map;

import lombok.Data;

@Data
public class RowModel {
    private Map<Integer, ColumnModel> rows;
}
