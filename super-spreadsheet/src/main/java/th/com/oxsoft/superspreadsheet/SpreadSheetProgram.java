package th.com.oxsoft.superspreadsheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * SpreadSheetProgram : platform for read excel file.
 */
public class SpreadSheetProgram {
    /**
     * Source file.
     */
    private File file;
    /**
     * Current position sheet. 
     */
    private int currentSheet;
    /**
     * Current position row.
     */
    private int currentRow;
    private int currentCell;
    private boolean isOpenFile;
    private boolean isProgramRunning;
    private FileInputStream fileInputStream;
    private XSSFWorkbook workbook;

    public boolean isProgramRunning(){
        return isProgramRunning;
    }
    
    public void openProgram(){
        this.isProgramRunning = true;
    }

    public int getCurrentSheet(){
        return this.currentSheet;
    }

    public int getCurrentRow(){
        return this.currentRow;
    }

    public int getCurrentCell(){
        return this.currentCell;
    }

    public boolean isFileOpening(){
        return this.isOpenFile;
    }

    public void openFile(File openFile) throws InvalidFormatException, IOException{
        this.file = openFile;
        fileInputStream = new FileInputStream(openFile);
        this.workbook = new XSSFWorkbook(file);
        this.currentSheet = 0;
        this.currentRow = 0;
        this.currentCell = 0;
        this.isOpenFile = true;
    }

    public Object readValue(){
        XSSFSheet sheet = this.workbook.getSheetAt(this.currentSheet);
        if(sheet == null)return null;
        Row row = sheet.getRow(this.currentRow);
        if(row == null)return null;
        Cell cell = row.getCell(this.currentCell);
        if(cell != null){
            switch (cell.getCellType()) 
            {
                case NUMERIC:
                    return cell.getNumericCellValue();
                case STRING:
                    return cell.getStringCellValue();
            }
        }
        return null;
    }

    public void nextSheet(){
        this.currentSheet++;
    }

    public void previousSheet(){
        this.currentSheet--;
    }

    public void nextRow(){
        this.currentRow++;
        this.currentCell = 0;
    }

    public void previousRow(){
        this.currentRow--;
        this.currentCell = 0;
    }

    public void nextColumn(){
        this.currentCell++;
    }

    public void previousColumn(){
        this.currentCell--;
    }

    public void closeFile(){
        if(this.fileInputStream != null){
            try {
                this.fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        if(this.workbook != null){
            try {
                this.workbook.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.isOpenFile = false;
    }

    public void closeProgram() {
        this.closeFile();
        this.isProgramRunning = false;
    }
}
