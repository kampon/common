package th.com.oxsoft.superspreadsheet;

import lombok.Data;

@Data
public class Spreadsheet {
    public enum SHEET_STATUS {
        OPEN,
        CLOSE
    }

    private String file;
    private int currentSheet;
    private int totalSheet;
    private String sheetStatus;

    public Spreadsheet(String file){
        this.file = file;
    }

    public void openFile(){

    }

    public void closeFile(){

    }

    public void nextSheet(){

    }

    public void previousSheet(){

    }

    public int getTotalSheet(){
        return this.totalSheet;
    }
}
