package th.com.oxsoft.superspreadsheet;

import java.io.File;

public class SuperSheet {
    private File sourceFile; 

    public SuperSheet(File file){
        this.sourceFile = file;
    }

    public String getSourceFileName(){
        return this.sourceFile.getName();
    }
}
