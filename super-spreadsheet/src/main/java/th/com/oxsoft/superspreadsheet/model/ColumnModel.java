package th.com.oxsoft.superspreadsheet.model;

import lombok.Data;

@Data
public class ColumnModel {
    private String name;
    private Object value;
}
