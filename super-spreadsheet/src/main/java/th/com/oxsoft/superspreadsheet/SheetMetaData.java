package th.com.oxsoft.superspreadsheet;

import lombok.Data;

/**
 * define sheet specification.
 */
@Data
public class SheetMetaData {
    /**
     * define sheet name
     */
    private String sheetName;

    /**
     * define column 
     * Example :
     * columns = {"Name", "Age", "Province"}
     */
    private String[] columnNames;

}
