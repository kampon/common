package th.com.oxsoft.superspreadsheet.model;

import java.util.Map;

import lombok.Data;

@Data
public class FileModel {
    /**
     * Sequence of sheet read follow by source file sequence.
     */
    private Map<Integer, SheetModel> sheets;
}
