package th.com.oxsoft.superspreadsheet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

public class SpreadSheetProgramTest {
    
    @Test
    public void test_SpreadSheetProgram_Case_OpenAndCloseProgram_Expected_ProgramOpenAndCloseCorrectly(){
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        assertEquals(true, spreadSheetProgram.isProgramRunning());
        spreadSheetProgram.closeProgram();
        assertEquals(false, spreadSheetProgram.isProgramRunning());
    }

    @Test
    public void test_SpreadSheetProgram_Case_OpenFile_Expected_ProgramCanOpenFileCorrectly() throws InvalidFormatException, IOException {
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
    }

    @Test
    public void test_SpreadSheetProgram_Case_CloseFile_Expected_ProgramCanCloseCompleted() throws InvalidFormatException, IOException {
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
        spreadSheetProgram.closeFile();
        spreadSheetProgram.closeProgram();
    }

    @Test
    public void test_SpreadSheetProgram_Case_CheckStatusOfSheet_Expected_correctState() throws InvalidFormatException, IOException {
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
        assertEquals(0, spreadSheetProgram.getCurrentSheet());
        assertEquals(0, spreadSheetProgram.getCurrentRow());
        assertEquals(0, spreadSheetProgram.getCurrentCell());
        spreadSheetProgram.closeFile();
        spreadSheetProgram.closeProgram();
    }

    @Test
    public void test_SpreadSheetProgram_Case_ReadData_Expected_CorrectData() throws InvalidFormatException, IOException {
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
        assertEquals("label_a", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("label_b", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("label_c", spreadSheetProgram.readValue());
        spreadSheetProgram.nextRow();
        assertEquals("data_1", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("data_2", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("data_3", spreadSheetProgram.readValue());
        spreadSheetProgram.closeFile();
        spreadSheetProgram.closeProgram();
    }

    @Test
    public void test_SpreadSheetProgram_Case_PreviousReadData_Expected_CorrectData() throws InvalidFormatException, IOException {
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
        assertEquals("label_a", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("label_b", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("label_c", spreadSheetProgram.readValue());
        spreadSheetProgram.nextRow();
        assertEquals("data_1", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("data_2", spreadSheetProgram.readValue());
        spreadSheetProgram.nextColumn();
        assertEquals("data_3", spreadSheetProgram.readValue());

        spreadSheetProgram.previousColumn();
        assertEquals("data_2", spreadSheetProgram.readValue());
        spreadSheetProgram.previousColumn();
        assertEquals("data_1", spreadSheetProgram.readValue());
        spreadSheetProgram.previousRow();
        assertEquals("label_a", spreadSheetProgram.readValue());
        
        spreadSheetProgram.closeFile();
        spreadSheetProgram.closeProgram();
    }

    @Test
    public void test_SpreadSheetProgram_Case_CheckOpenFileStatus_Expected_CorrectlyStatus() throws InvalidFormatException, IOException{
        SpreadSheetProgram spreadSheetProgram = new SpreadSheetProgram();
        spreadSheetProgram.openProgram();
        assertFalse("After open program, file status should be close", spreadSheetProgram.isFileOpening());
        spreadSheetProgram.openFile(TestConstant.TEST_FILE);
        assertTrue("After open file, status should be open", spreadSheetProgram.isFileOpening());
        spreadSheetProgram.closeFile();
        assertFalse("After close file, status should be close.", spreadSheetProgram.isFileOpening());
    }
}
