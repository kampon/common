package th.com.oxsoft.superspreadsheet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

import org.junit.Test;

/**
 * Unit test for Spreadsheet.
 */
public class SpreadsheetTest 
{
    /**
     * Spreadsheet Test :-)
     */
    @Test
    public void test_Spreadsheet_Case_CheckFileTestIsExisting_Expected_FileIsExisting()
    {
        assertTrue("sheet_with_label.xlsx not found.", TestConstant.TEST_FILE.exists());   
    }

    @Test
    public void test_Spreadsheet_Case_GetSourceFileName_Expected_CorrectFileName() {
        URL url = getClass().getResource("./document/sheet_with_label.xlsx");
        SuperSheet sheet = new SuperSheet(TestConstant.TEST_FILE);
        assertEquals(sheet.getSourceFileName(), "sheet_with_label.xlsx");
    }
}
