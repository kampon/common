package th.com.oxsoft.superspreadsheet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SheetMetaDataTest {

    @Test
    public void test_SheetMetaData_Case_CreateNewSheetMetadata_Expected_NewSheetMetaDataObject() {
        SheetMetaData sheetMetaData = new SheetMetaData();
        sheetMetaData.setSheetName("sheetName");
        String[] columnNames = {"name", "age", "province"};
        sheetMetaData.setColumnNames(columnNames);

        assertEquals("sheetName", sheetMetaData.getSheetName());
        assertArrayEquals(columnNames, sheetMetaData.getColumnNames());
        
    }
}
